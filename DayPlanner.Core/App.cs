using Cirrious.CrossCore.IoC;
using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.ViewModels;

namespace DayPlanner.Core
{
    public class App : MvxApplication
    {
        public override void Initialize()
        {
            CreatableTypes()
                .EndingWith("Service")
                .AsInterfaces()
                .RegisterAsLazySingleton();

            RegisterAppStart<MainScreenViewModel>();
        }
    }
}