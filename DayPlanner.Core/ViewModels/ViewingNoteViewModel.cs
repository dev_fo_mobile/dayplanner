﻿using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.SqlServices;
using System.Windows.Input;

namespace DayPlanner.Core.ViewModels
{
    public class ViewingNoteViewModel : MvxViewModel
    {
        private readonly IDataService _dataService;
        private Note _note;

        public Note Note
        {
            get { return _note; }
            set { _note = value; RaisePropertyChanged(() => Note); }
        }

        #region Commands

        // Переход на экран редактирования заметки
        public ICommand EditNoteCommand { get; set; }

        // Удаление заметки
        public ICommand DeleteNoteCommand { get; set; }

        // Переход на главный экран 
        public ICommand ComeBackCommand { get; set; }

        // Обновление заметки
        public ICommand UpdateNoteCommand { get; set; }

        #endregion

        public ViewingNoteViewModel(IDataService DataService)
        {
            _dataService = DataService;
            EditNoteCommand = new MvxCommand(CreateNote);
            DeleteNoteCommand = new MvxCommand(DeleteNote);
            ComeBackCommand = new MvxCommand(() => Close(this));
            UpdateNoteCommand = new MvxCommand(UpdateNote);
        }

        public void CreateNote()
        {
            var id = Note.Id;
            ShowViewModel<CreateNoteViewModel>(new { index = id });
        }

        public void DeleteNote()
        {
            _dataService.Delete(Note);
            Close(this);
        }

        public void UpdateNote()
        {
            this.Note = _dataService.GetNote(this.Note.Id);
        }

        public void Init(int index)
        {
            this.Note = _dataService.GetNote(index);
        }

        protected override void InitFromBundle(IMvxBundle parameters)
        {
            base.InitFromBundle(parameters);
            
            int index = int.Parse(parameters.Data["index"]);
            this.Note = _dataService.GetNote(index);
        }
    }
}
