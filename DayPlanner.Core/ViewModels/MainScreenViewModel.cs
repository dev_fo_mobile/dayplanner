using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.SqlServices;
using System;
using System.Collections.Generic;
using System.Windows.Input;

namespace DayPlanner.Core.ViewModels
{
    public class MainScreenViewModel
        : MvxViewModel
    {
        // ������������� ������ ��� ������ � ��
        private readonly IDataService _dataService;
        private List<Note> _notes;
        private static DateTime _date;
        private Note _selectedNote;

        #region Properties

        // �������, ��������� �� ���� �� ����(SelectedDate)
        public List<Note> Notes 
        {
            get { return _notes; }
            set { _notes = value; RaisePropertyChanged(() => Notes); }
        }

        // ����, �������������� �� ������� ������, �� ������� �� ���� ���������� �������
        public DateTime SelectedDate
        {
            get { return _date; }
            set
            {
                _date = value;
                ApplyFilterCommand.Execute(null);
                RaisePropertyChanged(() => SelectedDate);
            }
        }

        // �������, � ������� �������� ��������( Click|LongClick )
        public Note SelectedNote
        {
            get { return _selectedNote; }
            set { _selectedNote = value; RaisePropertyChanged(() => SelectedNote); }
        }

        #endregion

        #region Commands

        // ������� �� ����� �������� �������
        public ICommand CreateNoteCommand { get; set; }

        // ������� �� ����� ���������
        public ICommand CalendarCommand { get; set; }

        // ������� �� ����� ��������
        public ICommand SettingsCommand { get; set; }

        // ������� �� ����� ��������� �������
        public ICommand ViewingNoteCommand { get; set; }

        // ������ ������� � ���� ������. ��������� ������� �� SelectedDate
        public ICommand ApplyFilterCommand { get; set; }

        #endregion

        public MainScreenViewModel(IDataService DataService)
        {
            _dataService = DataService;
            if (_date == DateTime.MinValue)
            {
                _date = DateTime.Today;
            }
            CreateNoteCommand = new MvxCommand(() => ShowViewModel<CreateNoteViewModel>(0));
            CalendarCommand = new MvxCommand(ShowCalendar);
            SettingsCommand = new MvxCommand(() => ShowViewModel<SettingsViewModel>());
            ViewingNoteCommand = new MvxCommand(() => ShowViewModel<ViewingNoteViewModel>(new { index = SelectedNote.Id }));
            ApplyFilterCommand = new MvxCommand(DoApplyFilter);
        }

        public void Init(DateTime date)
        {
            if (date != DateTime.MinValue)
            {
                SelectedDate = date;
            }
        }

        public static void ChangeDate(DateTime newDate)
        {
            _date = newDate;
        }

        public void ShowCalendar()
        {
            ShowViewModel<CalendarViewModel>(new { date = SelectedDate });
        }

        private void DoApplyFilter()
        {
            Notes = _dataService.NotesMatching(this.SelectedDate);
        }
    }
}
