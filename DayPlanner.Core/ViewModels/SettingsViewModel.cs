﻿using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.SqlServices;
using System.Windows.Input;

namespace DayPlanner.Core.ViewModels
{
    public class SettingsViewModel : MvxViewModel
    {
        public ICommand ComeBackCommand { get; set; }

        public SettingsViewModel(IDataService DataService)
        {
            ComeBackCommand = new MvxCommand(() => Close(this));
        }
    }
}
