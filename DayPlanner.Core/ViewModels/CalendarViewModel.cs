﻿using Cirrious.MvvmCross.ViewModels;
using System;
using System.Windows.Input;

namespace DayPlanner.Core.ViewModels
{
   public class CalendarViewModel : MvxViewModel
   {
       public static DateTime Date;

       public ICommand ShowMainScreanCommand { get; set; }

       public ICommand ComeBackCommand { get; set; }

       public CalendarViewModel()
       {
           ShowMainScreanCommand = new MvxCommand<DateTime>(ShowMainScreen);
           ComeBackCommand = new MvxCommand(() => Close(this));
       }

       private void ShowMainScreen(DateTime newDate)
       {
           Date= newDate;
           MainScreenViewModel.ChangeDate(newDate);
           Close(this);
       }

       public DateTime GetDate()
       {
           return Date;
       }

       public void Init(DateTime date)
       {
           if (date != DateTime.MinValue)
           {
               Date = date;
           }
       }
   }
}
