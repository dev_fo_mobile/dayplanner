﻿using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.SqlServices;
using System.Windows.Input;

namespace DayPlanner.Core.ViewModels
{
    public class CreateNoteViewModel : MvxViewModel
    {
        private readonly IDataService _dataService;

        private int _noteId;
        private string _noteText;
        private Note _note;

        #region Properties

        public int NoteId
        {
            get { return _noteId; }
        }

        public string NoteText
        {
            get { return _noteText; }
            set { _noteText = value; RaisePropertyChanged(() => NoteText); }
        }

        public Note Note
        {
            get { return _note; }
            set { _note = value; RaisePropertyChanged(() => Note); }
        }

        #endregion

        #region Commands

        public ICommand InsertCommand { get; set; }

        public ICommand UpdateCommand { get; set; }

        public ICommand BackCommand { get; set; }

        #endregion

        public CreateNoteViewModel(IDataService DataService)
        {
            _dataService = DataService;
            BackCommand = new MvxCommand(() => Close(this));
            UpdateCommand = new MvxCommand(UpdateNote);
            InsertCommand = new MvxCommand(InsertToDataBase);
        }

        private void InsertToDataBase()
        {
            if (Note != null)
                _dataService.Insert(Note);
        }

        private void UpdateNote()
        {
            _dataService.Update(Note);
        }

        public void Init(int index)
        {
            if (index!=0)
            {
                Note = _dataService.GetNote(index);
                NoteText = Note.NoteText;
            }
            _noteId = index;
        }
    }
}
