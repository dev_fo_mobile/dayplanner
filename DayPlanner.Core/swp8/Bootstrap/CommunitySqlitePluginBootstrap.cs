using Cirrious.CrossCore.Plugins;

namespace DayPlanner.Core.Bootstrap
{
    public class SqlitePluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Community.Plugins.Sqlite.PluginLoader>
    {
    }
}