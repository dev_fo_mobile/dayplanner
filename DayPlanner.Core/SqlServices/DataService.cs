﻿using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayPlanner.Core.SqlServices
{
    public class DataService : IDataService
    {
        private readonly ISQLiteConnection _connection;

        public DataService(ISQLiteConnectionFactory factory)
        {
            _connection = factory.Create("one.sql");
            _connection.CreateTable<Note>();
        }

        public List<Note> NotesMatching(DateTime date)
        {
            string dateStr = string.Format("{0}.{1}.{2}", 
                                            date.Day.ToString().PadLeft(2, '0'), 
                                            date.Month.ToString().PadLeft(2, '0'), 
                                            date.Year);
            return _connection.Table<Note>().OrderBy(x => x.Date)
                                            .Where(x => x.DateString == dateStr)
                                            .ToList();
        }

        public Note GetNote(int id)
        {
            return _connection.Get<Note>(x => x.Id == id);
        }

        public void Insert(Note note)
        {
            _connection.Insert(note);
        }

        public void Update(Note note)
        {
            _connection.Update(note);
        }

        public void Delete(Note note)
        {
            _connection.Delete(note);
        }

        public List<Note> GetNotificationNotes(DateTime time)
        {
            string textDate = string.Format("{0}.{1}.{2}", 
                                            time.Day.ToString().PadLeft(2, '0'), 
                                            time.Month.ToString().PadLeft(2, '0'), 
                                            time.Year);
            string textTime = string.Format("{0}:{1}", 
                                            time.Hour.ToString().PadLeft(2, '0'), 
                                            time.Minute.ToString().PadLeft(2, '0'));

            return _connection.Table<Note>().Where(x => x.RemindDateString == textDate && 
                                                   x.RemindTimeString == textTime &&
                                                   x.Notification == true).ToList();  
        }

        public int Count
        {
            get
            {
                return _connection.Table<Note>().Count();
            }
        }
    }
}
