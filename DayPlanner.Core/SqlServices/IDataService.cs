﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayPlanner.Core.SqlServices
{
    public interface IDataService
    {
        List<Note> NotesMatching(DateTime date);
        Note GetNote(int id);
        void Insert(Note note);
        void Update(Note note);
        void Delete(Note note);
        int Count { get; }
        List<Note> GetNotificationNotes(DateTime time);
    }
}
