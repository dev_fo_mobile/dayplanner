﻿using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DayPlanner.Core.SqlServices
{
    public class Note
    {
        #region PropertiesAndFields

        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        private DateTime _date;
        public DateTime Date
        {
            get { return _date; }
            set
            {
                _date = value;
                UpdateDateString();
            } 
        }

        private DateTime _remindTime;
        public DateTime RemindTime
        {
            get { return _remindTime; }
            set 
            {
                _remindTime = value;
                UpdateRemindTimeString();
            }
        }

        public string TimeString { get; set; }

        public string DateString { get; set; }

        public string RemindTimeString { get; set; }

        public string RemindDateString { get; set; }

        public bool Notification { get; set; }

        public string NoteText { get; set; }

        #endregion PropertiesAndFields

        public Note() { }

        public Note(DateTime date, DateTime remindTime, bool notification, string noteText)
        {
            Date = date;
            RemindTime = remindTime;
            Notification = notification;
            NoteText = noteText;
        }

        private void UpdateDateString()
        {
            TimeString = string.Format("{0}:{1}", 
                                        Date.Hour.ToString().PadLeft(2, '0'), 
                                        Date.Minute.ToString().PadLeft(2, '0'));
            DateString = string.Format("{0}.{1}.{2}", 
                                        Date.Day.ToString().PadLeft(2, '0'), 
                                        Date.Month.ToString().PadLeft(2, '0'), 
                                        Date.Year);
        }

        private void UpdateRemindTimeString()
        {
            RemindTimeString = string.Format("{0}:{1}",
                                        RemindTime.Hour.ToString().PadLeft(2, '0'),
                                        RemindTime.Minute.ToString().PadLeft(2, '0'));
            RemindDateString = string.Format("{0}.{1}.{2}",
                                        RemindTime.Day.ToString().PadLeft(2, '0'),
                                        RemindTime.Month.ToString().PadLeft(2, '0'),
                                        RemindTime.Year);
        }
    }
}
