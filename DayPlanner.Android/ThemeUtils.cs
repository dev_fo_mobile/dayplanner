using Android.App;
using Android.Content;

namespace DayPlanner.Android
{
    class ThemeUtils
    {
        public static int cTheme;
        public static int Light = 0;
        public static int  Dark = 1;
        

        public static void changeToTheme(Activity activity, int theme)
        {
            cTheme = theme;
            activity.Finish();
           
            activity.StartActivity(new Intent(activity, activity.Class));
        }

        public static void onActivityCreateSetTheme(Activity activity)
        {
            switch (cTheme)
            {
                default:
                case 0:
                    activity.SetTheme(Resource.Style.CustomLightTheme);
                    break;
                case 1:
                    activity.SetTheme(Resource.Style.CustomDarkTheme);
                    break;               
            }
        }
    }
}