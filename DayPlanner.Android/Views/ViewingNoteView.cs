using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using DayPlanner.Android.ActionBar;
using DayPlanner.Core.ViewModels;

namespace DayPlanner.Android.Views
{
    [Activity(Label = "�������� �������")]
    public class ViewingNoteView : MvxActionBarActivity
    {
        private ClipboardManager _clipBoard;

        protected override void OnCreate(Bundle bundle)
        {
            ThemeUtils.onActivityCreateSetTheme(this);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.ViewingNoteView);     
            var textViewTime = (TextView)FindViewById(Resource.Id.textView2);
            var textViewDate = (TextView)FindViewById(Resource.Id.textView1);
            var textViewNoteText = (TextView)FindViewById(Resource.Id.textView3);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            _clipBoard = (ClipboardManager)GetSystemService(ClipboardService);
        }

        protected override void OnStart()
        {
            base.OnStart();
            ((ViewingNoteViewModel)ViewModel).UpdateNoteCommand.Execute(null);
        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.viewingNote_menu, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                // ���� �� "��������"
                case Resource.Id.change_note:
                    ((ViewingNoteViewModel)ViewModel).EditNoteCommand.Execute(null);
                    break;
                // ���� �� "�����������"
                case Resource.Id.copy_note:
                    _clipBoard.PrimaryClip = ClipData.NewPlainText("", ((ViewingNoteViewModel)ViewModel).Note.NoteText);
                    break;
                // ���� �� "�������"
                case Resource.Id.del_note:
                    ((ViewingNoteViewModel)ViewModel).DeleteNoteCommand.Execute(null);
                    break;
                // ���� �� "������� �����"              
                case global::Android.Resource.Id.Home:
                    ((ViewingNoteViewModel)ViewModel).ComeBackCommand.Execute(null);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

    }
}