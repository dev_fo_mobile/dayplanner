using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Views;
using DayPlanner.Android.ActionBar;
using DayPlanner.Core.ViewModels;
using Java.IO;
using System;

namespace DayPlanner.Android.Views
{
    [Activity(Label = "���������")]
    class SettingsView : MvxActionBarActivity
    {
        Button melodyButton;
        Button themeButton;
        Dialog dialog;

        private static string melodyText = "1 melody";
        private static string themeText = "������� ����";

        const int melodyDialogId = 0;
        const int themeDialogId = 1;

        protected override void OnCreate(Bundle bundle)
        {
            ThemeUtils.onActivityCreateSetTheme(this);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.SettingsView);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            melodyButton = (Button)FindViewById(Resource.Id.button1);
            themeButton = (Button)FindViewById(Resource.Id.btnViewTheme);

            melodyButton.Click += (o, e) => ShowDialog(melodyDialogId);
            themeButton.Click += (o, e) => ShowDialog(themeDialogId);

            UpdateDisplay();
        }
        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                // ���� �� "������� �����"              
                case global::Android.Resource.Id.Home:
                    ((SettingsViewModel)ViewModel).ComeBackCommand.Execute(null);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        private void UpdateDisplay()
        {
            melodyButton.Text = melodyText;
            themeButton.Text = themeText;
        }

        protected override Dialog OnCreateDialog(int id)
        {
            switch (id)
            {
                case melodyDialogId:
                    var alertDialog = new AlertDialog.Builder(this)
                        .SetTitle("����� �������")
                        .SetCancelable(false)
                        .SetNegativeButton("������", (EventHandler<DialogClickEventArgs>)null)
                        .SetPositiveButton("��", OnClickOK)
                        .SetSingleChoiceItems(Resource.Array.melodies, 0, MelodySet);
                    dialog = alertDialog.Create();
                    break;
                case themeDialogId:
                    alertDialog = new AlertDialog.Builder(this)
                        .SetTitle("����� ����")                        
                        .SetSingleChoiceItems(Resource.Array.themes, 0, ThemeSet);
                    dialog = alertDialog.Create();
                    break;
                default:
                    dialog = null;
                    break;
            }
            return dialog;
        }

        private void MelodySet(object sender, DialogClickEventArgs e)
        {
            var strings = Resources.GetStringArray(Resource.Array.melodies);
            melodyText = strings[e.Which];
        }

        private void ThemeSet(object sender, DialogClickEventArgs e)
        {
            var strings = Resources.GetStringArray(Resource.Array.themes);
            switch (e.Which)
            {
                case 0:
                    ThemeUtils.changeToTheme(this, ThemeUtils.Light);                   
                    break;
                case 1:                    
                    ThemeUtils.changeToTheme(this, ThemeUtils.Dark);                    
                    break;
                default:
                    break;
            }
            themeText = strings[e.Which];
        }

        private void OnClickOK(object sender, DialogClickEventArgs e)
        {
            UpdateDisplay();
        }
        
    }
}