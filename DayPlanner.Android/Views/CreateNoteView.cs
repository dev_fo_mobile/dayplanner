using Android.App;
using Android.Content;
using Android.OS;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Views;
using DayPlanner.Core.SqlServices;
using DayPlanner.Core.ViewModels;
using System;

namespace DayPlanner.Android.Views
{
    [Activity(Label = "�������� �������")]
    public class CreateNoteView : MvxActivity
    {
        TextView textViewTime;
        TextView textViewDate;
        TextView textViewReminder;

        ImageButton save;
        ImageButton cancel;

        private int hour;
        private int minute;
        private DateTime date;
        private string reminder = "�� ����������";
        private string tempReminder = "�� ����������";

        const int TIME_DIALOG_ID = 0; //����������� �����, ���������� ���������������� ���������� ����
        const int DATE_DIALOG_ID = 1;
        const int REMINDER_DIALOG_ID = 2;

        protected override void OnCreate(Bundle bundle)
        {
            ThemeUtils.onActivityCreateSetTheme(this);

            base.OnCreate(bundle);
            SetContentView(Resource.Layout.CreateNoteView);

            textViewTime = (TextView)FindViewById(Resource.Id.textViewTime);
            textViewDate = (TextView)FindViewById(Resource.Id.textViewDate);
            textViewReminder = (TextView)FindViewById(Resource.Id.textViewReminder);

            save = (ImageButton)FindViewById(Resource.Id.buttonSave);
            cancel = (ImageButton)FindViewById(Resource.Id.buttonCancel);

            // ������� click listener �� TextView
            textViewTime.Click += (o, e) => ShowDialog(TIME_DIALOG_ID);
            textViewDate.Click += (o, e) => ShowDialog(DATE_DIALOG_ID);
            textViewReminder.Click += (o, e) => ShowDialog(REMINDER_DIALOG_ID);

            save.Click += (o, e) => { OnClickSave(); };
            cancel.Click += (o, e) => { OnClickCancel(); };

            // ������� ������� ����� � ����

            if (((CreateNoteViewModel)ViewModel).NoteId != 0)
            {
                this.hour = ((CreateNoteViewModel)ViewModel).Note.Date.Hour;
                this.minute = ((CreateNoteViewModel)ViewModel).Note.Date.Minute;
                this.date = ((CreateNoteViewModel)ViewModel).Note.Date.Date;
            }
            else
            {
                hour = DateTime.Now.Hour;
                minute = DateTime.Now.Minute;
                date = DateTime.Today;

            }
            UpdateDisplay();
        }

        // ��������� �����, ����, ����������� � ������� �� �� TextView
        private void UpdateDisplay()
        {
            string time = string.Format("{0}:{1}", hour, minute.ToString().PadLeft(2, '0'));
            textViewTime.Text = time;

            textViewDate.Text = date.ToString("d MMM yyyy");
            textViewReminder.Text = reminder;

        }

        // �����, ������� ����� ����������, ����� ������������ ������������� ����� �����
        private void OnTimeSet(object sender, TimePickerDialog.TimeSetEventArgs e)
        {
            this.hour = e.HourOfDay;
            this.minute = e.Minute;
            UpdateDisplay();
        }

        // �����, ������� ����� ����������, ����� ������������ ������������� ����� ����
        void OnDateSet(object sender, DatePickerDialog.DateSetEventArgs e)
        {
            this.date = e.Date;
            UpdateDisplay();
        }

        // �����, ������� ����� ����������, ����� ������������ ������������� ����� �����������
        void OnReminderSet(object sender, DialogClickEventArgs e)
        {
            var strings = Resources.GetStringArray(Resource.Array.reminder_strings);
            tempReminder = strings[e.Which];

        }

        // �����, ������� ����� ����������, ����� ������������ ������ �� �� ��� ������ �����������
        void OnClickOK(object sender, DialogClickEventArgs e)
        {
            this.reminder = tempReminder;
            UpdateDisplay();
        }

        // ����� ��������� ������
        protected override Dialog OnCreateDialog(int id)
        {
            Dialog dialog;
            switch (id)
            {
                case DATE_DIALOG_ID:
                    dialog = new DatePickerDialog(this, OnDateSet, date.Year, date.Month - 1, date.Day);

                    break;
                case TIME_DIALOG_ID:
                    dialog = new TimePickerDialog(this, OnTimeSet, hour, minute, true);
                    break;
                case REMINDER_DIALOG_ID:
                    var alertDialog = new AlertDialog.Builder(this);
                    alertDialog.SetTitle("��������� ��...");
                    alertDialog.SetCancelable(false);
                    alertDialog.SetNegativeButton("������", (EventHandler<DialogClickEventArgs>)null);
                    alertDialog.SetPositiveButton("��", OnClickOK);
                    alertDialog.SetSingleChoiceItems(Resource.Array.reminder_strings, 0, OnReminderSet);
                    dialog = alertDialog.Create();

                    break;
                default:
                    dialog = null;
                    break;
            }
            return dialog;
        }

        // ����� ����� ���������� ��� ������� �� ������ "��" ��� �������� �������
        public void OnClickSave()
        {
            this.date = this.date.AddHours(hour);
            this.date = this.date.AddMinutes(minute);    
            if (((CreateNoteViewModel)ViewModel).NoteId == 0)
            {
                bool remind = true;
                DateTime remindTime = this.date;
                if (((CreateNoteViewModel)ViewModel).NoteText == null)
                {
                    ((CreateNoteViewModel)ViewModel).NoteText = "������ �������";
                }
                if (reminder == "�� ����������")
                {
                    remind = false;
                }

                else
                {
                    remindTime = Remind(this.date);
                }

                ((CreateNoteViewModel)ViewModel).Note = new Note(date, remindTime, remind, ((CreateNoteViewModel)ViewModel).NoteText);
                ((CreateNoteViewModel)ViewModel).InsertCommand.Execute(null);
            }

            else
            {
                ((CreateNoteViewModel)ViewModel).Note.NoteText = ((CreateNoteViewModel)ViewModel).NoteText;
                ((CreateNoteViewModel)ViewModel).Note.Date = this.date;
                if (reminder == "�� ����������")
                {
                    ((CreateNoteViewModel)ViewModel).Note.Notification = false;
                }
                else
                {
                    ((CreateNoteViewModel)ViewModel).Note.RemindTime = Remind(this.date);
                }
                ((CreateNoteViewModel)ViewModel).UpdateCommand.Execute(null);
            }
            ((CreateNoteViewModel)ViewModel).BackCommand.Execute(null);
        }

        private void OnClickCancel()
        {
            ((CreateNoteViewModel)ViewModel).BackCommand.Execute(null);
        }

        private DateTime Remind(DateTime date)
        {
            DateTime remindTime = date;
            switch (reminder)
            {
                case "5 �����":
                    remindTime = date.Subtract(new TimeSpan(0, 5, 0));
                    break;
                case "10 �����":
                    remindTime = date.Subtract(new TimeSpan(0, 10, 0));
                    break;
                case "15 �����":
                    remindTime = date.Subtract(new TimeSpan(0, 15, 0));
                    break;
                case "30 �����":
                    remindTime = date.Subtract(new TimeSpan(0, 30, 0));
                    break;
                case "1 ��c":
                    remindTime = date.Subtract(new TimeSpan(1, 0, 0));
                    break;
                case "2 ����":
                    remindTime = date.Subtract(new TimeSpan(2, 0, 0));
                    break;
                case "3 ����":
                    remindTime = date.Subtract(new TimeSpan(3, 0, 0));
                    break;
                case "4 ����":
                    remindTime = date.Subtract(new TimeSpan(4, 0, 0));
                    break;
                case "5 �����":
                    remindTime = date.Subtract(new TimeSpan(5, 0, 0));
                    break;
                case "12 �����":
                    remindTime = date.Subtract(new TimeSpan(12, 0, 0));
                    break;
                case "1 ����":
                    remindTime = date.Subtract(new TimeSpan(1, 0, 0, 0));
                    break;
                case "2 ���":
                    remindTime = date.Subtract(new TimeSpan(2, 0, 0, 0));
                    break;
                case "3 ���":
                    remindTime = date.Subtract(new TimeSpan(3, 0, 0, 0));
                    break;
                case "1 ������":
                    remindTime = date.Subtract(new TimeSpan(7, 0, 0, 0));
                    break;
                case "2 ������":
                    remindTime = date.Subtract(new TimeSpan(14, 0, 0, 0));
                    break;
                case "3 ������":
                    remindTime = date.Subtract(new TimeSpan(21, 0, 0, 0));
                    break;
                case "1 �����":
                    remindTime = date.Subtract(new TimeSpan(30, 0, 0, 0));
                    break;
                case "1 ���":
                    remindTime = date.Subtract(new TimeSpan(365, 0, 0, 0));
                    break;
                default:
                    break;
            }

            return remindTime;
        }

    }

}