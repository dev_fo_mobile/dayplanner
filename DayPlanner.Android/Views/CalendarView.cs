using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Views;
using DayPlanner.Android.ActionBar;
using DayPlanner.Android.Adapters;
using DayPlanner.Core.ViewModels;
using Java.Text;
using Java.Util;
using System;
using System.Collections.Generic;

namespace DayPlanner.Android.Views
{
    [Activity(Label = "���������")]
    public class CalendarView : MvxActionBarActivity
    {
        private readonly SimpleDateFormat _dateFormat = new SimpleDateFormat("MMM. yyyy");

        private GridCellAdapter _adapter;
        private GridView _calendarView;
        private TextView _currentMonth;
        private Calendar _calendar;
        private DateTime _selecterDate;
        private int _month;
        private int _year;

        public DateTime SelectedDate
        {
            get { return _selecterDate; }
            set
            {
                _selecterDate = value;
                _month = _selecterDate.Month - 1;
                _year = _selecterDate.Year;
            }
        }

        protected override void OnCreate(Bundle bundle)
        {
            ThemeUtils.onActivityCreateSetTheme(this);
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.CalendarView);
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);

            _calendar = Calendar.GetInstance(Locale.Default);
            SelectedDate = ((CalendarViewModel)ViewModel).GetDate();
            _calendar.Set(SelectedDate.Year, SelectedDate.Month - 1, SelectedDate.Day);

            InitViews();
            _month = SelectedDate.Month - 1;
            _year = SelectedDate.Year;
            _adapter = new GridCellAdapter(Application.Context, this, SelectedDate, _month, _year);
            _calendarView.Adapter = _adapter;
        }

        private void InitViews()
        {
            var prevMonth = (ImageView)FindViewById(Resource.Id.prevMonth);
            prevMonth.Click += PrevMonth_Click;

            var nextMonth = (ImageView)FindViewById(Resource.Id.nextMonth);
            nextMonth.Click += NextMonth_Click;

            _currentMonth = (TextView)FindViewById(Resource.Id.currentMonth);
            _currentMonth.Text = _dateFormat.Format(_calendar.Time);
            _calendarView = (GridView)FindViewById(Resource.Id.calendar);
        }

        private void SetGridCellAdapterToDate(int month, int year)
        {
            _adapter = new GridCellAdapter(Application.Context, this, SelectedDate, month, year);
            _calendar.Set(year, month, _calendar.Get(CalendarField.DayOfMonth));
            _currentMonth.Text = _dateFormat.Format(_calendar.Time);
            _calendarView.Adapter = _adapter;
        }

        public void PrevMonth_Click(object sender, EventArgs e)
        {
            if (_month == 0)
            {
                _month = 11;
                _year--;
            }
            else _month--;
            SetGridCellAdapterToDate(_month, _year);
        }

        public void NextMonth_Click(object sender, EventArgs e)
        {
            if (_month == 11)
            {
                _month = 0;
                _year++;
            }
            else _month++;
            SetGridCellAdapterToDate(_month, _year);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId)
            {
                // ���� �� "������� �����"              
                case global::Android.Resource.Id.Home:
                    ((CalendarViewModel)ViewModel).ComeBackCommand.Execute(null);
                    break;
            }
            return base.OnOptionsItemSelected(item);
        }

        public void GridCellClick(DateTime newSelectedDate)
        {
            ((CalendarViewModel)ViewModel).ShowMainScreanCommand.Execute(newSelectedDate);
            this.Finish();
        }

    }
}