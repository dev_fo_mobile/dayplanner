using Android.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Cirrious.MvvmCross.Binding.Droid.Views;
using DayPlanner.Android.ActionBar;
using DayPlanner.Android.Services;
using DayPlanner.Core.ViewModels;


namespace DayPlanner.Android.Views
{
    [Activity(Label = "DayPlanner")]
    public class MainScreenView : MvxActionBarActivity
    {
        int temp = 0;
        MvxListView _lv;

        protected override void OnCreate(Bundle bundle)
        {
            ThemeUtils.onActivityCreateSetTheme(this);
            
            base.OnCreate(bundle);
            SetContentView(Resource.Layout.MainScreenView);
            
            var str = ((MainScreenViewModel)ViewModel).SelectedDate.ToString("d MMM yyyy");            
            this.Title = str;

            _lv = FindViewById<MvxListView>(Resource.Id.listview);
            _lv.ItemClick = ((MainScreenViewModel)ViewModel).ViewingNoteCommand;
        }

        protected override void OnStart()
        {
            if (temp == 0)
            {
                temp++;
            }
            else
            {
                ThemeUtils.changeToTheme(this, ThemeUtils.cTheme);
            }           
          
            base.OnStart();
            ((MainScreenViewModel)ViewModel).ApplyFilterCommand.Execute(null);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            this.MenuInflater.Inflate(Resource.Menu.main_menu, menu);            
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {           
            switch (item.ItemId)
            {
                // ���� �� "+". ��������� ������� �� MainScreenViewModel
                case Resource.Id.action_add_note:                    
                    ((MainScreenViewModel)ViewModel).CreateNoteCommand.Execute(null);
                    break;

                case Resource.Id.action_calendar:
                    ((MainScreenViewModel)ViewModel).CalendarCommand.Execute(null);
                    break;

                case Resource.Id.action_settings:
                    ((MainScreenViewModel)ViewModel).SettingsCommand.Execute(null);
                    break;

                case Resource.Id.action_exit:
                    this.Finish();
                    closeBackgroundService();
                    break;                
            }
            return base.OnOptionsItemSelected(item);
        }

        // ��������� �������
        private void closeBackgroundService()
        {
            var context = Application.Context;
            context.StopService(new Intent(context, typeof(ServiceForNotifications)));
        }
    }
}