using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cirrious.MvvmCross.Droid.Platform;
using Cirrious.CrossCore;
using DayPlanner.Android;

namespace DayPlanner.Android.Services
{
    // ������ ������� ��� �������� ����������
    [BroadcastReceiver(Enabled = true, Exported = true)]
    [IntentFilter(new [] { Intent.ActionBootCompleted })]
    public class BootBroadcast : BroadcastReceiver
    {
        public override void OnReceive(Context context, Intent intent) 
        {
            Mvx.Trace("BootBroadcast started");
            // ������������� ���������� ��� ����� �������� �������
            var setup = MvxAndroidSetupSingleton.EnsureSingletonAvailable(context);
            setup.EnsureInitialized();
            // ������ �������
            //context.StartService(new Intent(context, typeof(ServiceForNotifications)));
        }
    }
}