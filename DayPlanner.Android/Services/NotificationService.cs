using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Cirrious.CrossCore;
using System.Threading;
using DayPlanner.Core.SqlServices;
using Android.Support.V7.App;
using Cirrious.CrossCore.Droid.Platform;
using Cirrious.MvvmCross.Droid.Platform;
using Cirrious.MvvmCross.Community.Plugins.Sqlite;
using Cirrious.CrossCore.Core;
using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Core.ViewModels;
using Cirrious.MvvmCross.Droid.Views;
using Android.Media;

namespace DayPlanner.Android.Services
{
    [Service]
    public class ServiceForNotifications : Service
    {
        private bool _exist = true;
        private readonly IDataService _dataService;
        private Thread _serviceThread;

        public ServiceForNotifications()
        {
            _dataService = Mvx.Resolve<IDataService>();
        }

        public override void OnCreate()
        {
            base.OnCreate();
        }

        // ������ �� ������ ������ �������
        public override StartCommandResult OnStartCommand(Intent intent, StartCommandFlags flags, int startId)
        {
            Mvx.Trace("NotificationService started");

            // ����� ����� ��� ������� ������ �������
            // (���� ��������� ������ � ������� ������, �� ����� Sleep ������� ���������� ��������)
            _serviceThread = new Thread(() =>
            {
                while (_exist)
                {
                    DoWork();
                    Thread.Sleep(60000 - DateTime.Now.Second*1000);
                }
            });
            _serviceThread.IsBackground = true;
            _serviceThread.Start();

            // ����� ����������� � ���, ��� ������ ��������
            // ��� ����� ������ ������� StartForeground
            Notification.Builder builder = new Notification.Builder(this)
                .SetContentTitle("Day Planner")
                .SetContentText("���������� �������� �  ������� ������")
                .SetSmallIcon(Resource.Drawable.ic_feedback_white_48dp);

            Notification notification = builder.Build();

            // ��� ��������� ��������� �������,
            // ��� ������ ����� ��������� ������ � ������� ������
            // �������� �������� ������
            StartForeground((int)NotificationFlags.ForegroundService, notification);

            // ��� ������� ��������, ��� ������ ��������������, ���� ������� ��� �������
            return StartCommandResult.Sticky;
        }

        // �������� ������ ��� ����������� �������
        public override void OnDestroy()
        {
            if (_serviceThread != null)
            {
                if (_serviceThread.IsAlive)
                    _serviceThread.Abort();
                _serviceThread = null;
            }
        }

        public override IBinder OnBind(Intent intent)
        {
            return null;
        }

        // �������� �����������
        private void DoWork()
        {
            List<Note> notes = _dataService.GetNotificationNotes(DateTime.Now);

            if (notes.Count > 0)
            {
                foreach (Note note in notes)
                    CallNotification(note);
            }
        }

        // ����� �����������
        private void CallNotification(Note note)
        {
            var request = MvxViewModelRequest<ViewingNoteViewModel>.GetDefaultRequest();
            request.ParameterValues = new Dictionary<string, string>();
            request.ParameterValues.Add("index", note.Id.ToString());

            var translator = Mvx.Resolve<IMvxAndroidViewModelRequestTranslator>();
            var intent = translator.GetIntentFor(request);
            var pending = PendingIntent.GetActivity(Application.Context, 0, intent, 0);

            var builder = new NotificationCompat.Builder(this)
                .SetContentTitle("������� � " + note.TimeString)
                .SetSmallIcon(Resource.Drawable.ic_description_white_48dp)
                .SetContentIntent(pending)
                .SetContentText(note.NoteText)
                .SetAutoCancel(true)
                .SetSound(RingtoneManager.GetDefaultUri(RingtoneType.Notification));

            var notificationManager = (NotificationManager)this.ApplicationContext.GetSystemService(Context.NotificationService);
            notificationManager.Notify(note.Id, builder.Build());
        } 
    }
}