using Cirrious.CrossCore.Plugins;

namespace DayPlanner.Android.Bootstrap
{
    public class JsonLocalisationPluginBootstrap
        : MvxPluginBootstrapAction<Cirrious.MvvmCross.Plugins.JsonLocalisation.PluginLoader>
    {
    }
}