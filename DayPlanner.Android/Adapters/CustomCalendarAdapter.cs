using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Java.Util;
using DayPlanner.Android.Views;
using Android.Widget;

namespace DayPlanner.Android.Adapters
{
    public class GridCellAdapter : BaseAdapter
    {
        private readonly int[] _daysOfMonth = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

        private DayPlanner.Android.Views.CalendarView _calendarView;
        private Context _context;
        private List<String> _list;
        private int _daysInMonth;
        private DateTime _selectedDate;
        private Button _gridcell;
        private int _year;
        private int _month;

        public override int Count
        {
            get { return _list.Count; }
        }

        public DateTime SelectedDate
        {
            get { return _selectedDate; }
            set { _selectedDate = value; }
        }

        public GridCellAdapter(Context context, DayPlanner.Android.Views.CalendarView calView, DateTime selectedDate, int month, int year)
            : base()
        {
            _context = context;
            _calendarView = calView;
            _list = new List<String>();
            _year = year;
            _month = month;
            _selectedDate = selectedDate;

            PrintMonth();
        }

        private void PrintMonth()
        {
            int daysInPrevMonth,
                prevMonth,
                prevYear,
                nextMonth,
                nextYear;

            _daysInMonth = _daysOfMonth[_month];

            switch (_month)
            {
                case 0: prevMonth = 11;
                    daysInPrevMonth = _daysOfMonth[prevMonth];
                    prevYear = _year - 1;
                    nextYear = _year;
                    nextMonth = 1;
                    break;

                case 11: prevMonth = _month - 1;
                    daysInPrevMonth = _daysOfMonth[prevMonth];
                    prevYear = _year;
                    nextYear = _year + 1;
                    nextMonth = 0;
                    break;

                default: prevMonth = _month - 1;
                    daysInPrevMonth = _daysOfMonth[prevMonth];
                    prevYear = _year;
                    nextYear = _year;
                    nextMonth = _month + 1;
                    break;
            }

            GregorianCalendar cal = new GregorianCalendar(_year, _month, 1);

            // ���������� ����, ����������� � ����������� ������. 
            // GregorianCalendar �������� ��� ������ �� 1 �� 7, ������� � �����������, ������� �������� 2(1 ��� ��������� � ���� � 1 ��� ����� � ������������).
            // ����� ������� �������� �������� �� 0 �� 5, ��� ������� ����� ���������� �������������� � �� �� ��. ���� ����� ���������� � ��, ����������� �������� 6
            int endOfPrevMonth = ((cal.Get(CalendarField.DayOfWeek) - 2) >= 0) ? cal.Get(CalendarField.DayOfWeek) - 2 : 6;

            if (cal.IsLeapYear(cal.Get(CalendarField.Year)))
            {
                switch (_month)
                {
                    case 2: ++_daysInMonth; break;
                    case 3: ++daysInPrevMonth; break;
                }
            }

            // ��� ����������� ������
            for (int i = 1; i <= endOfPrevMonth; i++)
            {
                _list.Add((daysInPrevMonth - endOfPrevMonth + i).ToString() + "-GREY" + "-PREV");
            }

            var today = DateTime.Today;
            // ��� �������� ������
            for (int i = 1; i <= _daysInMonth; i++)
            {

                if (i == SelectedDate.Day && _month == SelectedDate.Month - 1 && _year == SelectedDate.Year)
                {
                    _list.Add(i.ToString() + "-BLUE" + "-CURRENT");
                }
                else
                {
                    if (i == today.Day && _month == today.Month - 1 && _year == today.Year)
                    {
                        _list.Add(i.ToString() + "-RED" + "-CURRENT");
                    }
                    else
                    {
                        _list.Add(i.ToString() + "-WHITE" + "-CURRENT");
                    }
                }
            }
            var nextMonthDays = 7 - ((endOfPrevMonth + _daysInMonth) % 7);
            // ��� ���������� ������
            for (int i = 1; i <= nextMonthDays; i++)
            {
                if (_month + 1 == 12)
                {
                    _list.Add(i.ToString() + "-GREY" + "-NEXT");
                }
                else
                {
                    _list.Add(i.ToString() + "-GREY" + "-NEXT");
                }
            }
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            var row = convertView;
            if (row == null)
            {
                LayoutInflater inflater = (LayoutInflater)_context.GetSystemService(Context.LayoutInflaterService);
                row = inflater.Inflate(Resource.Layout.ScreenGridCell, parent, false);
            }

            var day_info = _list[position].Split('-');
            _gridcell = (Button)row.FindViewById(Resource.Id.calendar_day_gridcell);
            _gridcell.Text = day_info[0];

            switch (day_info[1])
            {
                case "GREY":
                    _gridcell.SetTextColor(_context.Resources.GetColor(Resource.Color.grey_color));
                    _gridcell.SetBackgroundDrawable(_context.Resources.GetDrawable(Resource.Drawable.apptheme_btn_default_holo_light));
                    break;

                case "WHITE":
                    if (ThemeUtils.cTheme == 0)
                    {
                        _gridcell.SetTextColor(_context.Resources.GetColor(Resource.Color.dark_color));
                    }
                    else
                    {
                        _gridcell.SetTextColor(_context.Resources.GetColor(Resource.Color.light_color));
                    }
                    _gridcell.SetBackgroundDrawable(_context.Resources.GetDrawable(Resource.Drawable.apptheme_btn_default_holo_dark));
                    break;

                case "RED":
                    _gridcell.SetTextColor(_context.Resources.GetColor(Resource.Color.red_color));
                    _gridcell.SetBackgroundDrawable(_context.Resources.GetDrawable(Resource.Drawable.apptheme_btn_default_holo_dark));
                    break;

                case "BLUE":
                    _gridcell.SetTextColor(_context.Resources.GetColor(Resource.Color.blue_color));
                    _gridcell.SetBackgroundDrawable(_context.Resources.GetDrawable(Resource.Drawable.apptheme_btn_default_holo_dark));
                    break;
            }

            switch (day_info[2])
            {
                case "PREV":
                    _gridcell.Click += PrevMonthCellClick;
                    break;
                case "NEXT":
                    _gridcell.Click += NextMonthCellClick;
                    break;
                default:
                    _gridcell.Click += CurrentMonthCellClick;
                    break;
            }
            return row;
        }

        private void CurrentMonthCellClick(object sender, EventArgs e)
        {
            _calendarView.GridCellClick(new DateTime(_year, _month + 1, int.Parse((sender as Button).Text)));
        }

        private void PrevMonthCellClick(object sender, EventArgs e)
        {
            _calendarView.GridCellClick(new DateTime(_year, _month, int.Parse((sender as Button).Text)));
        }

        private void NextMonthCellClick(object sender, EventArgs e)
        {
            _calendarView.GridCellClick(new DateTime(_year, _month + 2, int.Parse((sender as Button).Text)));
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return (Java.Lang.Object)_list[position];
        }
    }
}