using Android.App;
using Android.Content;
using Cirrious.CrossCore.Platform;
using Cirrious.MvvmCross.Droid.Platform;
using Cirrious.MvvmCross.ViewModels;
using DayPlanner.Android.Services;
using DayPlanner.Core;

namespace DayPlanner.Android
{
    public class Setup : MvxAndroidSetup
    {
        public Setup(Context applicationContext) : base(applicationContext)
        {
        }

        protected override IMvxApplication CreateApp()
        {
            return new App();
        }
		
        protected override IMvxTrace CreateDebugTrace()
        {
            return new DebugTrace();
        }

        protected override void InitializeLastChance()
        {
            // ����� ������� �����������
            var context = Application.Context;
            var intent = new Intent(Application.Context, typeof(ServiceForNotifications));
            context.StartService(intent);
            base.InitializeLastChance();
        }
    }
}